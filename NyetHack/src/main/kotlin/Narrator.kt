import kotlin.random.Random
import kotlin.random.nextInt

var narrationModifier: (String) -> String = { it }

fun changeNarratorMood() : Unit {
    val mood: String
    val modifier: (String) -> String

    when(Random.nextInt(1..4)){
        1 -> {
            mood = "loud"
            modifier = { message ->
                val numExclamationPoints = 3
                message.uppercase()+"!".repeat(numExclamationPoints)
            }
        }
        2 -> {
            mood = "tired"
            modifier = { message ->
                message.lowercase().replace(" ", "... ")
            }
        }
        3 -> {
            mood = "unsure"
            modifier = { message ->
                "$message?"
            }
        }
        4 -> {
            mood = "lazy"
            modifier = { message: String ->
                message.substring(0,message.length/2)
            }
        }
        else -> {
            mood = "professional"
            modifier = { message ->
                "$message."
            }
        }
    }
    narrationModifier = modifier
    narrate("The narrator beings to feel $mood")
}
//
//val narrationModifier: (String) -> String = { message ->
//    val numExclamationPoints = 3
//    message.uppercase()+"!".repeat(numExclamationPoints)
//}
//
//val narrationModifier2 = { message: String ->
//    val numExclamationPoints = 3
//    message.uppercase()+"!".repeat(numExclamationPoints)
//}

val loudNarration: (String, String) -> String = { message, tone ->
    when(tone){
        "excited" -> {
            val numExclamationPoints = 3
            message.uppercase() + "!".repeat(numExclamationPoints)
        }
        "sneaky" -> {
            "$message. The narrator has just blown Madrigal's cover.".uppercase()
        }
        else -> message.uppercase()
    }
}
inline fun narrate(
    message: String,
    modifier: (String) -> String = { narrationModifier(it)}
){
    println(modifier(message))
}