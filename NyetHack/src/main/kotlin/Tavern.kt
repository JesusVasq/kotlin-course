import java.io.File
import kotlin.random.Random
import kotlin.random.nextInt

private const val TAVERN_MASTER = "Taernyl"
private const val TAVERN_NAME = "$TAVERN_MASTER's Folly"

private val menuData = File("data/tavern-menu-data.txt").readText().split("\n").map{it.split(",")}
//private val menuItems = List(menuData.size){ index ->
//    val (type, name, price) = menuData[index].split(",")
//    name
//}
//private val menuItems: List<String> = menuData.map { menuEntry: String ->
//    val(_,name,_) = menuEntry.split(",")
//    name
//}
private val menuItems = menuData.map{ (_, name ,_) -> name}
//private val menuItemPrices: Map<String, Double> = List(menuData.size){ index ->
//    val (_, name, price) = menuData[index].split(",")
//    name to price.toDouble()
//}.toMap()
//private val menuItemPrices = menuData.associate{
//    val(_, name, price) = it.split(",")
//    name to price.toDouble()
//}
private val menuItemPrices = menuData.associate { (_, name, price) ->
    name to price.toDouble()
}
//private val menuItemTypes: Map<String, String> = List(menuData.size){ index ->
//    val (type, name, _)= menuData[index].split(",")
//    name to type
//}.toMap()
//private val menuItemTypes = menuData.associate{
//    val (type, name, _) = it.split(",")
//    name to type
//}
private val menuItemTypes = menuData.associate { (type, name, _) ->
    name to type
}

private val firstNames = setOf("Alex", "Mordoc", "Sophie", "Tariq")
private val lastNames = setOf("Ironfoot", "Fernworth", "Baggins", "Downstrider")

fun visitTavern() {
    narrate("${player.name} enters $TAVERN_NAME")
    narrate("There are several items for sale:")
//    println(menuItems)
    println(menuItems.joinToString())
//    val patrons = mutableListOf("Eli", "Mordoc", "Sophie")
//    val patrons: MutableSet<String> = mutableSetOf()
    val patrons: MutableSet<String> = firstNames.shuffled()
        .zip(lastNames.shuffled()) { firstName, lastName -> "$firstName $lastName" }
        .toMutableSet()
//    val patronGold = mapOf(
//        TAVERN_MASTER to 86.00,
//        heroName to 4.50
//    )
    val patronGold = mutableMapOf(
        TAVERN_MASTER to 86.00,
        player.name to 4.50,
        *patrons.map { it to 6.00 }.toTypedArray()
    )
//    repeat(10){
//        patrons += "${firstNames.random()} ${lastNames.random()}"
//    }
//    val eliMessage= if(patrons.contains("Eli")){
//        "$TAVERN_MASTER says: Eli's in the back is playing cards"
//    } else {
//        "$TAVERN_MASTER says: Eli isn't here"
//    }
//
//    val otherMessage: String = if(patrons.containsAll(listOf("Sophie","Mordoc"))){
//        "$TAVERN_MASTER says: Sophie and Mordoc are seated by the stew kettle"
//    } else {
//        "$TAVERN_MASTER says: Sophie and Mordoc aren't with each other right now"
//    }
//    println(eliMessage)
//    println(otherMessage)

//    narrate("Eli leaves the tavern")
//    patrons.remove("Eli")
//    narrate("Alex enters the tavern")
//    patrons.add("Alex")
//    println(patrons)
//    narrate("Alex (VIP) enters the tavern")
//    patrons[0] = "Alexis"
//    println(patrons)
//    for(patron in patrons){
//        println("Good evening, $patron")
//    }

//    patrons.forEachIndexed { index, patron ->
//        println("Good evening, $patron - you're #${index + 1} in line")
//        placeOrder(patron, menuItems.random())
//    }
//    menuData.forEachIndexed { index, data ->
//        println("$index : $data")
//    }

//    repeat(3){
//        placeOrder(patrons.random(), menuItems.random())
//    }
//    while(patrons.size < 5){
////        patrons += "${firstNames.random()} ${lastNames.random()}"
//        val patronName = "${firstNames.random()} ${lastNames.random()}"
//        patrons += patronName
//        patronGold += patronName to 6.0
//    }
//    patrons.forEach { patronName ->
//        patronGold += patronName to 6.0
//    }
    narrate("${player.name} sees several patrons in the tavern:")
    narrate(patrons.joinToString())
//    val favoriteItems = patrons.flatMap{ getFavoriteMenuItems(it)}
//    println("Favorite items: $favoriteItems")
    val itemOfDay = patrons.flatMap { getFavoriteMenuItems(it) }.random()
    narrate("The item of the day is the $itemOfDay")
//    println(patronGold)
    repeat(3) {
        placeOrder(patrons.random(), menuItems.random(), patronGold)
    }
//    println(patronGold)
    displayPatronBalances(patronGold)
//    val departingPatrons: List<String> = patrons
//        .filter { patron -> patronGold.getOrDefault(patron, 0.0) < 4.0 }
//    patrons -= departingPatrons
//    patronGold -= departingPatrons
//    departingPatrons.forEach { patron ->
//        narrate("$heroName sees $patron departing the tavern")
//    }
    patrons.filter { patron -> patronGold.getOrDefault(patron, 0.0) < 4.0 }
        .also { departingPatrons ->
            patrons -= departingPatrons
            patronGold -= departingPatrons
        }
        .forEach{
            patron -> narrate("${player.name} sees $patron departing the tavern")
        }
    narrate("There are still some patrons in the tavern")
    narrate(patrons.joinToString())

}

private fun getFavoriteMenuItems(patron: String): List<String> {
    return when(patron){
        "Alex Ironfoot" -> menuItems.filter { menuItem ->
            menuItemTypes[menuItem]?.contains("dessert") == true
        }
        else -> menuItems.shuffled().take(Random.nextInt(1..2))
    }
}

private var placeOrder: (String, String, MutableMap<String, Double>) -> Unit = { patronName, menuItemName, patronGold ->
    val itemPrice = menuItemPrices.getValue(menuItemName)
    narrate("$patronName speaks with $TAVERN_MASTER to place an order")
    if(itemPrice <= patronGold.getOrDefault(patronName, 0.0)){
//        narrate("$TAVERN_MASTER hands $patronName a $menuItemName")
        val action = when(menuItemTypes[menuItemName]){
            "shandy", "elixer" -> "pours"
            "meal" -> "serves"
            else -> "hands"
        }
        narrate("$TAVERN_MASTER $action $patronName a $menuItemName")
        narrate("$patronName pays $TAVERN_MASTER $itemPrice gold")
        patronGold[patronName] = patronGold.getValue(patronName) - itemPrice
        patronGold[TAVERN_MASTER] = patronGold.getValue(TAVERN_MASTER) + itemPrice
    }
    else{
        narrate("$TAVERN_MASTER says, \"You need more coin for a $menuItemName\"")
    }
}

private fun displayPatronBalances(patronGold: Map<String, Double>) {
    narrate("${player.name} intuitively knows how much money each patron has")
    patronGold.forEach { (patron, balance) ->
        narrate("$patron has ${"%.2f".format(balance)} gold")
    }
}