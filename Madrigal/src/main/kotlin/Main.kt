import java.lang.IllegalArgumentException

const val HERO_NAME: String = "Madrigal"
var playerLevel: Int = 4

fun main(args: Array<String>) {
    var hasSteed: Boolean = false
    var pubName: String = "The Unicorn's Horn"
    var currentPublican: String = "Bob"
    var gold: Int = 50
    var choices: List<String> = mutableListOf<String>("mead", "wine", "LaCroix")
    val hasAngeredBarbarians: Boolean = false
    val playerClass: String = "paladin"
    val hasBefriendedBarbarians: Boolean = true
    val race = "gnome"
    val faction: String = when (race) {
        "dwarf" -> "Keepers of the Mines"
        "gnome" -> "Tinkerers of the Underground"
        "orc", "human" -> "Free People of the Rolling Hills"
        else -> "Shadow Cabal of the Unseen Realm" // Unknown race
    }

    println("$HERO_NAME announces her presence to the world.")
    println("What level is $HERO_NAME?")
    playerLevel = readLine()?.toIntOrNull()?:0
    println("$HERO_NAME's level is $playerLevel.")
    println("Time passes...")
    playerLevel += 1
    println(playerLevel)
    readBountyBoard()

}



private fun obtainQuest(
    playerLevel: Int,
    playerClass: String = "paladin",
    hasBefriendedBarbarians: Boolean = true,
    hasAngeredBarbarians: Boolean = false
    ): String? {
    require (playerLevel <= 0 ){
         "The player's level must be at least 1"
    }
    return when (playerLevel) {
        1 -> "Meet Mr. Bubbles in the land of soft things."
        in 2..5 -> {
            val canTalkToBarbarians = !hasAngeredBarbarians &&
                    (hasBefriendedBarbarians || playerClass == "barbarian")
            if (canTalkToBarbarians) {
                "Convince the barbarians to call off their invasion."
            } else {
                "Save the town from the barbarian invasions."
            }
        }

        6 -> "Locate the enchanted sword."
        7 -> "Recover the long-lost artifact of creation."
        8 -> "Defeat Nogartse, bringer of death and eater of worlds."
        else -> null
    }
    }

private fun readBountyBoard() {
    val quest: String? = obtainQuest(playerLevel)
    val message: String = try{
        quest?.replace("Nogartse", "xxxxxxxx")
        ?.let{censoredQuest ->
            """
    |$HERO_NAME approaches the bounty board. It reads:
    || "$censoredQuest"
    """.trimMargin()
        }?:"$HERO_NAME appraoches the bounty board but it is blank"
    }
    catch(e: Exception){
        "$HERO_NAME can't read what's on the bounty board."
    }
    println(message)
}